module gitlab.com/SkynetLabs/skyfleet

go 1.16

require (
	gitlab.com/NebulousLabs/Sia v1.5.6-0.20210408043250-c949c8cd5bca
	gitlab.com/NebulousLabs/errors v0.0.0-20200929122200-06c536cf6975
	gitlab.com/SkynetLabs/skyd v1.5.6-0.20210416084031-19c0985b6a29
)
