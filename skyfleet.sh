#!/bin/bash

# build the aggregator
go build || exit 1

mkdir -p live/ maintenance/ dev/ history/

# Copy siaj to the live servers.
echo copying siaj to live servers
for server in eu-fin-1 eu-fin-2 eu-fin-3 eu-ger-1 eu-ger-2 eu-ger-3 eu-ger-4 eu-ger-5 eu-ger-6 eu-ger-7 eu-ger-8 eu-pol-1 eu-pol-2 eu-pol-3 us-or-1 us-or-2 us-pa-1 us-pa-2 us-va-1; do
	# progress statement
	echo copying siaj to $server
	# copy siaj to server
	scp /home/user/dev/bin/skyc $server.siasky.net:/home/user/siaj > /dev/null
	ssh $server.siasky.net docker cp /home/user/siaj sia:/usr/bin/siaj
done
#END

# Copy siaj to the dev servers.
echo copying siaj to dev servers
for server in siasky.dev siasky.xyz; do
	# progress statement
	echo copying siaj to $server
	# copy siaj to server
	scp /home/user/dev/bin/skyc $server:/home/user/siaj > /dev/null
	ssh $server docker cp /home/user/siaj sia:/usr/bin/siaj
done
#END

# Iterate over the live servers.
for server in eu-fin-1 eu-fin-2 eu-fin-3 eu-ger-1 eu-ger-2 eu-ger-3 eu-ger-4 eu-ger-5 eu-ger-6 eu-ger-7 eu-ger-8 eu-pol-1 eu-pol-2 eu-pol-3 us-or-1 us-or-2 us-pa-1 us-pa-2 us-va-1; do
	# progress statement
	echo scanning $server
	# grab the json dump from siaj on $server
	ssh $server.siasky.net docker exec sia siaj -s json > live/$server.json
done

# Iterate over the dev servers.
for server in siasky.dev siasky.xyz; do
	# progress statement
	echo scanning $server
	# grab the json dump from siaj on $server
	ssh $server docker exec sia siaj -s json > dev/$server.json
done

# run the aggregator and build the dashboard
./skyfleet | tee history/$(date +%F)
