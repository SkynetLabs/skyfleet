// fleet-metrics is a program that combines a bash script with a go program to
// generate a dashboard showing off a number of metrics for a portal fleet.
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/NebulousLabs/Sia/modules"
	"gitlab.com/NebulousLabs/Sia/types"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
)

// These variables are used to govern the expected state of the system. They
// should be updated as the fleet characteristics change.
var (
	// lowAllowanceThreshold establishes the threshold for having a low
	// allowance. The threshold is Numerator/Denominator. So 10/100 means that
	// when less than 10% of the allowance is 'UnspentUnallocated', the alert
	// will be presented.
	lowAllowanceThresholdNumerator = types.NewCurrency64(10)
	lowAllowanceThresholdDenom = types.NewCurrency64(100)

	// The min and max values for MaxStoragePrice specify in cents the valid
	// range for the maxstorageprice setting of the allowance. A node will be
	// reported if the max storage price of that node is not within this range.
	// A minMax of 200 and a maxMax of 500 indicate that the USD price for max
	// storage of a node must be between $2/TB/Mo and $5/TB/Mo in USD.
	minMaxStoragePrice = types.NewCurrency64(200)
	maxMaxStoragePrice = types.NewCurrency64(500)

	// allowanceNewUploadBuffer is the amount of money that should be set aside
	// in the allowance for new uploads.
	allowanceNewUploadBuffer  = types.NewCurrency64(30e3).Mul(types.SiacoinPrecision)

	// highAllowanceThreshold establishes the threshold for having a high
	// allowance. The threshold is Numerator/Denominator. So 2/1 means that the
	// allowance is reported as being 'high' when it is twice the amount of
	// unspent unallocated.
	highAllowanceThresholdNumerator = types.NewCurrency64(2)
	highAllowanceThresholdDenominator = types.NewCurrency64(1)

	// lowBalanceThreshold establishes the threshold for a low balance. 40/100
	// means that a server will be reported as having a low balance if the total
	// number of coins is the wallet is less than 40% of the total amount of
	// spending on contracts thus far.
	lowBalanceThresholdNumerator = types.NewCurrency64(40)
	lowBalanceThresholdDenominator = types.NewCurrency64(100)

	// vLowBalanceThreshold establishes the threshold for a very low balance.
	// 10/100 means that a server will be reported as having a low balance if
	// the total number of coins is the wallet is less than 10% of the total
	// amount of spending on contracts thus far.
	vLowBalanceThresholdNumerator = types.NewCurrency64(10)
	vLowBalanceThresholdDenominator = types.NewCurrency64(100)

	// highBalanceThreshold establishes the threshold for reporting a node as
	// having a high balance in the wallet. This is determined to be the case if
	// the total number of coins in the wallet is more than Num/Denom times the
	// allowance. For 2/1, it means that a node is reported if the total number
	// of coins in the wallet is more than double the allowance.
	highBalanceThresholdNumerator = types.NewCurrency64(2)
	highBalanceThresholdDenominator = types.NewCurrency64(1)

	// highRepairBurdenThreshold is the point at which the fleet manager will
	// report a server as having fallen behind on repairs.
	highRepairBurdenThreshold = uint64(1 << 40) // 1 TiB

	// tooMuchStorageThreshold is the point at which a node is reported for
	// having too much data renewing in its contracts. The node should be
	// rotated to maintenance.
	//
	// The maintenance threshold establishes that there is too much data in
	// renewing contracts even for a maintenance node, and that either siad
	// needs to be upgrades to manage more data, or some of the storage needs to
	// be offloaded to another siad. Try to avoid getting into that situation.
	tooMuchStorageThreshold = uint64(100e12)
	tooMuchStorageMaintenanceThreshold = uint64(200e12)

	// healthCheckLateThreshold is the threshold for reporting that a node is
	// behind on its health checks. A perfectly on-time node should have a
	// health check around 8 hours behind the present, and a node is probably
	// unhealthy and behind at around 24 hours.
	healthCheckLateThreshold = time.Minute * 60 * 24
)

// RenterFleetStats contains aggregated metrics across a fleet of renter nodes.
// Where the fields match, they are just a sum of that field across all of the
// RawStats.
type RenterFleetStats struct {
	// RawStats contains the RenterStats for each renter in the fleet.
	RawStats []skymodules.RenterStats

	DownloadThroughput15Min float64 // total bytes in 15 minutes
	UploadThroughput15Min   float64 // total bytes in 15 minutes
	RegistryReads15Min      float64 // total reads in 15 minutes
	RegistryWrites15Min     float64 // total writes in 15 minutes

	ActiveContractData  uint64
	PassiveContractData uint64
	WastedContractData  uint64

	AggregateLastHealthCheckTime time.Time
	TotalRepairSize              uint64
	TotalSiadirs                 uint64
	TotalSiafiles                uint64
	TotalSize                    uint64
	TotalStuckChunks             uint64
	TotalStuckSize               uint64

	TotalContractSpentFunds     types.Currency
	TotalContractSpentFees      types.Currency
	TotalContractRemainingFunds types.Currency

	TotalAllowance   types.Currency
	TotalWalletFunds types.Currency

	// These fields track which nodes have run out of memory.
	LiveOutOfMemory                []string
	LiveOutOfPriorityMemory        []string
	MaintenanceOutOfMemory         []string
	MaintenanceOutOfPriorityMemory []string
}

// unmarshalIntoRFS will unmarshal a marshaled skymodules.RenterStats and then
// combine it into the aggregated stats.
func unmarshalIntoRFS(rfs *RenterFleetStats, renterStatsBytes []byte, name string) (skymodules.RenterStats, error) {
	// Unmarshal the rs bytes and add them to the as raw.
	var rs skymodules.RenterStats
	err := json.Unmarshal(renterStatsBytes, &rs)
	if err != nil {
		return skymodules.RenterStats{}, errors.AddContext(err, "unable to unmarshal file")
	}
	rs.Name = strings.TrimSuffix(name, ".json")
	rfs.RawStats = append(rfs.RawStats, rs)

	// Add up all the throughput stats.
	rfs.DownloadThroughput15Min += rs.SkynetPerformance.Download64KB.FifteenMinutes.TotalSize
	rfs.DownloadThroughput15Min += rs.SkynetPerformance.Download1MB.FifteenMinutes.TotalSize
	rfs.DownloadThroughput15Min += rs.SkynetPerformance.Download4MB.FifteenMinutes.TotalSize
	rfs.DownloadThroughput15Min += rs.SkynetPerformance.DownloadLarge.FifteenMinutes.TotalSize
	rfs.UploadThroughput15Min += rs.SkynetPerformance.Upload4MB.FifteenMinutes.TotalSize
	rfs.UploadThroughput15Min += rs.SkynetPerformance.UploadLarge.FifteenMinutes.TotalSize
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N60ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N120ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N240ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N500ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N1000ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N2000ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N5000ms
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.N10s
	rfs.RegistryReads15Min += rs.SkynetPerformance.RegistryRead.FifteenMinutes.NLong
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N60ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N120ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N240ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N500ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N1000ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N2000ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N5000ms
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.N10s
	rfs.RegistryWrites15Min += rs.SkynetPerformance.RegistryWrite.FifteenMinutes.NLong

	// Aggregate the stats into the whole.
	rfs.ActiveContractData += rs.ActiveContractData
	rfs.PassiveContractData += rs.PassiveContractData
	rfs.WastedContractData += rs.WastedContractData

	// Consider the last health check time only if it is worse.
	if rs.AggregateLastHealthCheckTime.Before(rfs.AggregateLastHealthCheckTime) {
		rfs.AggregateLastHealthCheckTime = rs.AggregateLastHealthCheckTime
	}

	rfs.TotalRepairSize += rs.TotalRepairSize
	rfs.TotalSiadirs += rs.TotalSiadirs
	rfs.TotalSiafiles += rs.TotalSiafiles
	rfs.TotalSize += rs.TotalSize
	rfs.TotalStuckChunks += rs.TotalStuckChunks
	rfs.TotalStuckSize += rs.TotalStuckSize

	rfs.TotalContractSpentFunds = rfs.TotalContractSpentFunds.Add(rs.TotalContractSpentFunds)
	rfs.TotalContractSpentFees = rfs.TotalContractSpentFees.Add(rs.TotalContractSpentFees)
	rfs.TotalContractRemainingFunds = rfs.TotalContractRemainingFunds.Add(rs.TotalContractRemainingFunds)

	rfs.TotalAllowance = rfs.TotalAllowance.Add(rs.Allowance.Funds)
	rfs.TotalWalletFunds = rfs.TotalWalletFunds.Add(rs.WalletFunds)

	// Print any alerts.
	if len(rs.Alerts) > 0 {
		fmt.Println("Alerts found for", name)
		modules.PrintAlerts(rs.Alerts, modules.SeverityCritical)
	}

	return rs, nil
}

func newRenterFleetStats() *RenterFleetStats {
	return &RenterFleetStats{
		// As the stats are compiled, the worst aggregate last health check time
		// is used. Need to initialize to the current time so that the worst
		// time is taken, otherwise the call will fail as the 0 time is earlier.
		AggregateLastHealthCheckTime: time.Now(),
	}
}

// main
func main() {
	// Grab the USD price of siacoin from the operator.
	var price uint64
	fmt.Print("Please enter the current USD price of siacoin in millicents.\nFor example, 5 cents would have input 5000: ")
	_, err := fmt.Scanf("%d", &price)
	fmt.Println()
	if err != nil {
		fmt.Println("Unable to parse value:", err)
		return
	}
	// Convert that to a min price for storage and a max price for storage.
	minMaxStoragePrice := minMaxStoragePrice.Mul64(1e3).Div64(price).Mul(types.SiacoinPrecision).Div(modules.BlockBytesPerMonthTerabyte)
	maxMaxStoragePrice := maxMaxStoragePrice.Mul64(1e3).Div64(price).Mul(types.SiacoinPrecision).Div(modules.BlockBytesPerMonthTerabyte)
	// Print the range to the user in cents.
	fmt.Println("Max storage siacoin price range: ", minMaxStoragePrice.Mul(modules.BlockBytesPerMonthTerabyte).HumanString(), " - ", maxMaxStoragePrice.Mul(modules.BlockBytesPerMonthTerabyte).HumanString())
	fmt.Println()

	// Grab all of the files in the live folder and pull the statistics
	// together.
	folders := []string{"live", "dev", "maintenance"}
	rfs := newRenterFleetStats()
	for _, folder := range folders {
		err := filepath.Walk(folder, func(path string, info os.FileInfo, err error) error {
			// Skip any directories.
			if info.IsDir() {
				return nil
			}

			rsBytes, err := ioutil.ReadFile(path)
			if err != nil {
				return errors.AddContext(err, "unable to read file")
			}
			rs, err := unmarshalIntoRFS(rfs, rsBytes, path)
			if err != nil {
				return errors.AddContext(err, "unable to unmarshal into RFS")
			}

			// Tally up any servers that are out of memory.
			if !rs.HasRenterMemory {
				rfs.LiveOutOfMemory = append(rfs.LiveOutOfMemory, path)
			}
			if !rs.HasPriorityRenterMemory {
				rfs.LiveOutOfPriorityMemory = append(rfs.LiveOutOfPriorityMemory, path)
			}
			return nil
		})
		if err != nil {
			fmt.Println("aggregation failed:", err.Error())
			os.Exit(1)
		}
	}

	// Enumerate the servers that have alerts.
	addSpace := false
	var hasAlerts []string
	var lowAllowance []string
	var highAllowance []string
	var badMaxStoragePrice []string
	var lowBalance []string
	var vLowBalance []string
	var highBalance []string
	var highRepair []string
	var highRawStorage []string
	var vHighRawStorage []string
	var lateHealthCheck []string
	var slowDownloads []string
	var slowUploads []string
	var slowRegRead []string
	var slowRegWrite []string
	for _, rs := range rfs.RawStats {
		// Check for critical alerts.
		if len(rs.Alerts) > 0 {
			hasAlerts = append(hasAlerts, rs.Name)
		}
		// Check for low allowance.
		if rs.AllowanceUnspentUnallocated.Mul(lowAllowanceThresholdDenom).Cmp(rs.Allowance.Funds.Mul(lowAllowanceThresholdNumerator)) < 0 {
			fmt.Println(rs.AllowanceUnspentUnallocated)
			fmt.Println(rs.AllowanceUnspentUnallocated.Mul(lowAllowanceThresholdDenom))
			fmt.Println(rs.Allowance.Funds)
			fmt.Println(rs.Allowance.Funds.Mul(lowAllowanceThresholdNumerator))
			lowAllowance = append(lowAllowance, rs.Name)
		}
		// Check for high allowance. To account for new uploads that we may
		// want, we don't consider the first 'allowanceNewUploadBuffer' of the
		// allowance.
		var spentOrAllocated types.Currency
		if rs.Allowance.Funds.Cmp(rs.AllowanceUnspentUnallocated) > 0 {
			spentOrAllocated = rs.Allowance.Funds.Sub(rs.AllowanceUnspentUnallocated)
		}
		if spentOrAllocated.Cmp(allowanceNewUploadBuffer) > 0 && rs.Allowance.Funds.Cmp(spentOrAllocated.Mul(highAllowanceThresholdNumerator).Div(highAllowanceThresholdDenominator)) > 0 {
			highAllowance = append(highAllowance, rs.Name)
		}
		if rs.Allowance.MaxStoragePrice.Cmp(maxMaxStoragePrice) > 0 || rs.Allowance.MaxStoragePrice.Cmp(minMaxStoragePrice) < 0 {
			badMaxStoragePrice = append(badMaxStoragePrice, rs.Name)
		}
		// Check for low wallet balance.
		if rs.WalletFunds.Mul(lowBalanceThresholdDenominator).Div(lowBalanceThresholdNumerator).Cmp(rs.TotalContractSpentFunds.Add(rs.TotalContractRemainingFunds)) < 0 {
			lowBalance = append(lowBalance, rs.Name)
		}
		// Check for very low wallet balance.
		if rs.WalletFunds.Mul(vLowBalanceThresholdDenominator).Div(vLowBalanceThresholdNumerator).Cmp(rs.TotalContractSpentFunds.Add(rs.TotalContractRemainingFunds)) < 0 {
			vLowBalance = append(vLowBalance, rs.Name)
		}
		// Check for high wallet balance.
		if rs.WalletFunds.Cmp(rs.Allowance.Funds.Mul(highBalanceThresholdNumerator).Div(highBalanceThresholdDenominator)) > 0 {
			highBalance = append(highBalance, rs.Name)
		}
		// Check for high repair burden.
		if rs.TotalRepairSize > highRepairBurdenThreshold {
			highRepair = append(highRepair, rs.Name)
		}
		// Check for a large amount of raw storage being managed in renewing
		// contracts.
		if rs.ActiveContractData + rs.PassiveContractData > tooMuchStorageThreshold && !strings.HasPrefix(rs.Name, "maintenance") {
			highRawStorage = append(highRawStorage, rs.Name)
		}
		// Check for a very large amount of raw storage being managed in
		// renewing contracts.
		if rs.ActiveContractData + rs.PassiveContractData > tooMuchStorageMaintenanceThreshold {
			vHighRawStorage = append(vHighRawStorage, rs.Name)
		}
		// Check for being behind on health checks.
		if rs.AggregateLastHealthCheckTime.Add(healthCheckLateThreshold).Before(time.Now()) {
			lateHealthCheck = append(lateHealthCheck, rs.Name)
		}

		// TODO: Create warnings for:
		// + upload 4 MB
		// + regread
		// + regwrite
		//
		// that report if a server is going below some chosen speed for more
		// than some chosen percentage of requests. Then we've got most of our
		// basic reporting numbers, and we can dive full into changing the
		// internal algos to go a lot faster.
		ttfb5m := rs.SkynetPerformance.TimeToFirstByte.FiveMinutes
		downloadTTFBu1s5m := ttfb5m.N60ms
		downloadTTFBu1s5m += ttfb5m.N120ms
		downloadTTFBu1s5m += ttfb5m.N240ms
		downloadTTFBu1s5m += ttfb5m.N500ms
		downloadTTFBu1s5m += ttfb5m.N1000ms
		downloadTTFBlong5m := ttfb5m.N2000ms
		downloadTTFBlong5m += ttfb5m.N5000ms
		downloadTTFBlong5m += ttfb5m.N10s
		downloadTTFBlong5m += ttfb5m.NLong
		downloadErr5m := ttfb5m.NErr
		downloadTTFB5mTotal := downloadTTFBu1s5m + downloadTTFBlong5m + downloadErr5m
		if downloadTTFBu1s5m/downloadTTFB5mTotal < 0.9 && downloadTTFB5mTotal > 1 {
			slowDownloads = append(slowDownloads, rs.Name)
		}
		upload4MB5m := rs.SkynetPerformance.Upload4MB.FiveMinutes
		upload4MBu10s5m := upload4MB5m.N60ms
		upload4MBu10s5m += upload4MB5m.N120ms
		upload4MBu10s5m += upload4MB5m.N240ms
		upload4MBu10s5m += upload4MB5m.N500ms
		upload4MBu10s5m += upload4MB5m.N1000ms
		upload4MBu10s5m += upload4MB5m.N2000ms
		upload4MBu10s5m += upload4MB5m.N5000ms
		upload4MBu10s5m += upload4MB5m.N10s
		upload4MBlong5m := upload4MB5m.NLong
		uploadErr5m := upload4MB5m.NErr
		upload4MB5mTotal := upload4MBu10s5m + upload4MBlong5m + uploadErr5m
		if upload4MBu10s5m/upload4MB5mTotal < 0.9 && upload4MB5mTotal > 1 {
			slowUploads = append(slowUploads, rs.Name)
		}
		regRead5m := rs.SkynetPerformance.RegistryRead.FiveMinutes
		regReadu5s5m := regRead5m.N60ms
		regReadu5s5m += regRead5m.N120ms
		regReadu5s5m += regRead5m.N240ms
		regReadu5s5m += regRead5m.N500ms
		regReadu5s5m += regRead5m.N1000ms
		regReadu5s5m += regRead5m.N2000ms
		regReadu5s5m += regRead5m.N5000ms
		regReadlong5m := regRead5m.N10s
		regReadlong5m += regRead5m.NLong
		regReadErr5m := regRead5m.NErr
		regRead5mTotal := regReadu5s5m + regReadlong5m + regReadErr5m
		if regReadu5s5m/regRead5mTotal < 0.9 && regRead5mTotal > 1 {
			slowRegRead = append(slowRegRead, rs.Name)
		}
		regWrite5m := rs.SkynetPerformance.RegistryWrite.FiveMinutes
		regWriteu10s5m := regWrite5m.N60ms
		regWriteu10s5m += regWrite5m.N120ms
		regWriteu10s5m += regWrite5m.N240ms
		regWriteu10s5m += regWrite5m.N500ms
		regWriteu10s5m += regWrite5m.N1000ms
		regWriteu10s5m += regWrite5m.N2000ms
		regWriteu10s5m += regWrite5m.N5000ms
		regWriteu10s5m += regWrite5m.N10s
		regWritelong5m := regWrite5m.NLong
		regWriteErr5m := regWrite5m.NErr
		regWrite5mTotal := regWriteu10s5m + regWritelong5m + regWriteErr5m
		if regWriteu10s5m/regWrite5mTotal < 0.9 && regWrite5mTotal > 1 {
			slowRegWrite = append(slowRegWrite, rs.Name)
		}
	}
	if len(hasAlerts) > 0 {
		fmt.Println("These servers have alerts:", hasAlerts)
		addSpace = true
	}
	if len(lowAllowance) > 0 {
		fmt.Println("These servers have spent >90% of their allowance:", lowAllowance)
		addSpace = true
	}
	if len(highAllowance) > 0 {
		fmt.Println("These servers have a high allowance:", highAllowance)
		addSpace = true
	}
	if len(badMaxStoragePrice) > 0 {
		fmt.Println("These servers have a bad max storage price:", badMaxStoragePrice)
		addSpace = true
	}
	if len(lowBalance) > 0 {
		fmt.Println("These servers have less than 40% of the contract spending remaining in the wallet:", lowBalance)
		addSpace = true
	}
	if len(vLowBalance) > 0 {
		fmt.Println("These servers have less than 10% of the contract spending remaining in the wallet:", vLowBalance)
		addSpace = true
	}
	if len(highBalance) > 0 {
		fmt.Println("These servers have more than twice the allowance in their wallets:", highBalance)
		addSpace = true
	}
	if len(highRepair) > 0 {
		fmt.Println("These servers currently have a high repair burden:", highRepair)
		addSpace = true
	}
	if len(highRawStorage) > 0 {
		fmt.Println("These servers currently have more than 100 TB repairing storage on them:", highRawStorage)
		addSpace = true
	}
	if len(vHighRawStorage) > 0 {
		fmt.Println("These servers currently have more than 200 TB repairing storage on them:", vHighRawStorage)
		addSpace = true
	}
	if len(lateHealthCheck) > 0 {
		fmt.Println("These servers are behind on health checks:", lateHealthCheck)
		addSpace = true
	}
	if len(slowDownloads) > 0 {
		fmt.Println("These servers are currently experiencing slow downloads:", slowDownloads)
		addSpace = true
	}
	if len(slowUploads) > 0 {
		fmt.Println("These servers are currently experiencing slow uploads:", slowUploads)
		addSpace = true
	}
	if len(slowRegRead) > 0 {
		fmt.Println("These servers are currently experiencing slow registry reads:", slowUploads)
		addSpace = true
	}
	if len(slowRegWrite) > 0 {
		fmt.Println("These servers are currently experiencing slow registry writes:", slowUploads)
		addSpace = true
	}

	// Log any servers that are currently out of memory.
	if len(rfs.LiveOutOfPriorityMemory) > 0 {
		fmt.Println("These live servers are out of priority memory:", rfs.LiveOutOfPriorityMemory)
		addSpace = true
	}
	if len(rfs.LiveOutOfMemory) > 0 {
		fmt.Println("These live servers are out of non-priority memory:", rfs.LiveOutOfMemory)
		addSpace = true
	}
	if len(rfs.MaintenanceOutOfPriorityMemory) > 0 {
		fmt.Println("These maintenance servers are out of priority memory:", rfs.MaintenanceOutOfPriorityMemory)
		addSpace = true
	}
	if len(rfs.MaintenanceOutOfMemory) > 0 {
		fmt.Println("These maintenance servers are out of non-priority memory:", rfs.MaintenanceOutOfMemory)
		addSpace = true
	}
	if addSpace {
		fmt.Println()
	}

	// Tally up the download statistics.
	var downloadTTFBu240ms24h float64
	var downloadTTFBu1s24h float64
	var downloadTTFBu10s24h float64
	var downloadTTFBlong24h float64
	var downloadErr24h float64
	for _, raw := range rfs.RawStats {
		sp := raw.SkynetPerformance
		downloadTTFBu240ms24h += sp.TimeToFirstByte.TwentyFourHours.N60ms
		downloadTTFBu240ms24h += sp.TimeToFirstByte.TwentyFourHours.N120ms
		downloadTTFBu240ms24h += sp.TimeToFirstByte.TwentyFourHours.N240ms
		downloadTTFBu1s24h += sp.TimeToFirstByte.TwentyFourHours.N500ms
		downloadTTFBu1s24h += sp.TimeToFirstByte.TwentyFourHours.N1000ms
		downloadTTFBu10s24h += sp.TimeToFirstByte.TwentyFourHours.N2000ms
		downloadTTFBu10s24h += sp.TimeToFirstByte.TwentyFourHours.N5000ms
		downloadTTFBu10s24h += sp.TimeToFirstByte.TwentyFourHours.N10s
		downloadTTFBlong24h += sp.TimeToFirstByte.TwentyFourHours.NLong
		downloadErr24h += sp.TimeToFirstByte.TwentyFourHours.NErr
	}
	download24hTotal := downloadTTFBu240ms24h + downloadTTFBu1s24h + downloadTTFBu10s24h + downloadTTFBlong24h + downloadErr24h
	downloadTTFBu240ms24h = downloadTTFBu240ms24h / download24hTotal
	downloadTTFBu1s24h = downloadTTFBu1s24h / download24hTotal
	downloadTTFBu10s24h = downloadTTFBu10s24h / download24hTotal
	downloadTTFBlong24h = downloadTTFBlong24h / download24hTotal
	downloadErr24h = downloadErr24h / download24hTotal

	// Tally up the upload stats.
	var upload4MBu10s24h float64
	var upload4MBlong24h float64
	var uploadErr24h float64
	for _, raw := range rfs.RawStats {
		sp := raw.SkynetPerformance
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N60ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N120ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N240ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N500ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N1000ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N2000ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N5000ms
		upload4MBu10s24h += sp.Upload4MB.TwentyFourHours.N10s
		upload4MBlong24h += sp.Upload4MB.TwentyFourHours.NLong
		uploadErr24h += sp.Upload4MB.TwentyFourHours.NErr
	}
	upload24hTotal := upload4MBu10s24h + upload4MBlong24h + uploadErr24h
	upload4MBu10s24h = upload4MBu10s24h / upload24hTotal
	upload4MBlong24h = upload4MBlong24h / upload24hTotal
	uploadErr24h = uploadErr24h / upload24hTotal

	// Tally up the registry read stats.
	var regReadU240ms24h float64
	var regReadU1s24h float64
	var regReadU10s24h float64
	var regReadLong24h float64
	var regReadErr24h float64
	for _, raw := range rfs.RawStats {
		sp := raw.SkynetPerformance
		regReadU240ms24h += sp.RegistryRead.TwentyFourHours.N60ms
		regReadU240ms24h += sp.RegistryRead.TwentyFourHours.N120ms
		regReadU240ms24h += sp.RegistryRead.TwentyFourHours.N240ms
		regReadU1s24h += sp.RegistryRead.TwentyFourHours.N500ms
		regReadU1s24h += sp.RegistryRead.TwentyFourHours.N1000ms
		regReadU10s24h += sp.RegistryRead.TwentyFourHours.N2000ms
		regReadU10s24h += sp.RegistryRead.TwentyFourHours.N5000ms
		regReadU10s24h += sp.RegistryRead.TwentyFourHours.N10s
		regReadLong24h += sp.RegistryRead.TwentyFourHours.NLong
		regReadErr24h += sp.RegistryRead.TwentyFourHours.NErr
	}
	regRead24hTotal := regReadU240ms24h + regReadU1s24h + regReadU10s24h + regReadLong24h + regReadErr24h
	regReadU240ms24h = regReadU240ms24h / regRead24hTotal
	regReadU1s24h = regReadU1s24h / regRead24hTotal
	regReadU10s24h = regReadU10s24h / regRead24hTotal
	regReadLong24h = regReadLong24h / regRead24hTotal
	regReadErr24h = regReadErr24h / regRead24hTotal

	// Tally up the registry write stats.
	var regWriteU500ms24h float64
	var regWriteU2s24h float64
	var regWriteU10s24h float64
	var regWriteLong24h float64
	var regWriteErr24h float64
	for _, raw := range rfs.RawStats {
		sp := raw.SkynetPerformance
		regWriteU500ms24h += sp.RegistryWrite.TwentyFourHours.N60ms
		regWriteU500ms24h += sp.RegistryWrite.TwentyFourHours.N120ms
		regWriteU500ms24h += sp.RegistryWrite.TwentyFourHours.N240ms
		regWriteU500ms24h += sp.RegistryWrite.TwentyFourHours.N500ms
		regWriteU2s24h += sp.RegistryWrite.TwentyFourHours.N1000ms
		regWriteU2s24h += sp.RegistryWrite.TwentyFourHours.N2000ms
		regWriteU10s24h += sp.RegistryWrite.TwentyFourHours.N5000ms
		regWriteU10s24h += sp.RegistryWrite.TwentyFourHours.N10s
		regWriteLong24h += sp.RegistryWrite.TwentyFourHours.NLong
		regWriteErr24h += sp.RegistryWrite.TwentyFourHours.NErr
	}
	regWrite24hTotal := regWriteU500ms24h + regWriteU2s24h + regWriteU10s24h + regWriteLong24h + regWriteErr24h
	regWriteU500ms24h = regWriteU500ms24h / regWrite24hTotal
	regWriteU2s24h = regWriteU2s24h / regWrite24hTotal
	regWriteU10s24h = regWriteU10s24h / regWrite24hTotal
	regWriteLong24h = regWriteLong24h / regWrite24hTotal
	regWriteErr24h = regWriteErr24h / regWrite24hTotal

	// Print out the aggregated stats as human readable numbers.
	fmt.Println("Download Throughput averaged over 15 minutes:", modules.BandwidthUnits(uint64(rfs.DownloadThroughput15Min*8/15/60)))
	fmt.Println("Upload Throughput averaged over 15 minutes:", modules.BandwidthUnits(uint64(rfs.UploadThroughput15Min*8/15/60)))
	fmt.Println("Registry Reads averaged over 15 minutes:", modules.AddCommas(uint64(rfs.RegistryReads15Min)))
	fmt.Println("Registry Writes averaged over 15 minutes:", modules.AddCommas(uint64(rfs.RegistryWrites15Min)))
	fmt.Println()
	fmt.Printf("24h Download TTFB <240ms:  %.2f%%\n", 100*downloadTTFBu240ms24h)
	fmt.Printf("24h Download TTFB <1s:  %.2f%%\n", 100*downloadTTFBu1s24h)
	fmt.Printf("24h Download TTFB <10s: %.2f%%\n", 100*downloadTTFBu10s24h)
	fmt.Printf("24h Download TTFB Long: %.2f%%\n", 100*downloadTTFBlong24h)
	fmt.Printf("24h Download Err Rate:  %.2f%%\n", 100*downloadErr24h)
	fmt.Println()
	fmt.Printf("24h Upload 4MB <10s: %.2f%%\n", 100*upload4MBu10s24h)
	fmt.Printf("24h Upload 4MB Long: %.2f%%\n", 100*upload4MBlong24h)
	fmt.Printf("24h Upload 4MB Err:  %.2f%%\n", 100*uploadErr24h)
	fmt.Println()
	fmt.Printf("24h Registry Read <240ms: %.2f%%\n", 100*regReadU240ms24h)
	fmt.Printf("24h Registry Read <1s:    %.2f%%\n", 100*regReadU1s24h)
	fmt.Printf("24h Registry Read <10s:   %.2f%%\n", 100*regReadU10s24h)
	fmt.Printf("24h Registry Read Long:   %.2f%%\n", 100*regReadLong24h)
	fmt.Printf("24h Registry Read Err:    %.2f%%\n", 100*regReadErr24h)
	fmt.Println()
	fmt.Printf("24h Registry Write <500ms:  %.2f%%\n", 100*regWriteU500ms24h)
	fmt.Printf("24h Registry Write <5s:  %.2f%%\n", 100*regWriteU2s24h)
	fmt.Printf("24h Registry Write <10s: %.2f%%\n", 100*regWriteU10s24h)
	fmt.Printf("24h Registry Write Long: %.2f%%\n", 100*regWriteLong24h)
	fmt.Printf("24h Registry Write Err:  %.2f%%\n", 100*regWriteErr24h)
	fmt.Println()
	fmt.Println("Active Contract Data: ", modules.FilesizeUnits(rfs.ActiveContractData))
	fmt.Println("Passive Contract Data:", modules.FilesizeUnits(rfs.PassiveContractData))
	fmt.Println("Wasted Contract Data: ", modules.FilesizeUnits(rfs.WastedContractData))
	fmt.Println("All Contract Data:    ", modules.FilesizeUnits(rfs.ActiveContractData+rfs.PassiveContractData+rfs.WastedContractData))
	fmt.Println()
	fmt.Println("Worst Health Time:  ", time.Since(rfs.AggregateLastHealthCheckTime).Round(time.Minute))
	fmt.Println("Total Repair:       ", modules.FilesizeUnits(rfs.TotalRepairSize))
	fmt.Println("Total Siadirs:      ", modules.AddCommas(rfs.TotalSiadirs))
	fmt.Println("Total Siafiles:     ", modules.AddCommas(rfs.TotalSiafiles))
	fmt.Println("Total Size:         ", modules.FilesizeUnits(rfs.TotalSize))
	fmt.Println("Total Stuck Chunks: ", modules.AddCommas(rfs.TotalStuckChunks))
	fmt.Println("Total Stuck Size:   ", modules.FilesizeUnits(rfs.TotalStuckSize))
	fmt.Println()
	fmt.Println("Total Spent Funds In Contracts:    ", modules.CurrencyUnits(rfs.TotalContractSpentFunds))
	fmt.Println("Contract Fee Spending:             ", modules.CurrencyUnits(rfs.TotalContractSpentFees))
	fmt.Println("Total Remaining Funds In Contracts:", modules.CurrencyUnits(rfs.TotalContractRemainingFunds))
	fmt.Println()
	fmt.Println("Total Allowance:          ", modules.CurrencyUnits(rfs.TotalAllowance))
	fmt.Println("Total Funds In Wallets:   ", modules.CurrencyUnits(rfs.TotalWalletFunds))
}
