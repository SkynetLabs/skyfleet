# skyfleet

Skyfleet is a tool for managing a fleet of servers that belong to a single
Skynet portal. It'll do some scans and provide metrics such as average upload
times, average download times, and give warnings about things like low account
balances or critical warnings in the alert systems of your nodes.

## UNSTABLE

Please note that this repo should currently be considered unstable. Sometimes it
will only run if you are using a specific, yet-unmerged branch of the skyd code.
It also at the moment hardcodes the list of servers, rather than loading them
dynamically.
